FROM node:20

#On crée un dossier app dans le conteneur
WORKDIR /app

COPY . .

RUN npm install

EXPOSE 3000

# Commande pour démarrer le serveur
CMD ["npm", "start"]
